// Q5 Group users based on their Programming language mentioned in their designation.

function groupUsersBasedOnPLanguage(users){
    if ((typeof (users) == 'object') && (!Array.isArray(users)) && (users != null)) {

        let pLanguages = ["Python", "Golang", "Javascript"];
        let groupedUsers = {};

        for(let pLanguage of pLanguages){
            for(let user in users){
                if((users[user].designation).includes(pLanguage)){
                    if(!groupedUsers[pLanguage]){
                        groupedUsers[pLanguage] = [];
                    }

                    groupedUsers[pLanguage].push(user);
                }
            }
        }

        return groupedUsers;
    }
    else{
        return [];
    }
}

module.exports = groupUsersBasedOnPLanguage;