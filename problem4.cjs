// Q4 Find all users with masters Degree.

function getUsersWithMastersDegree(users){
    if ((typeof (users) == 'object') && (!Array.isArray(users)) && (users != null)) {
        let usersWithMastersDegree = [];
        for(let user in users){
            if((users[user].qualification).includes("Masters")){
                usersWithMastersDegree.push(user);
            }
        }
        return usersWithMastersDegree;
    }
    else{
        return [];
    }
}

module.exports = getUsersWithMastersDegree;