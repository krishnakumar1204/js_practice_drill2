// Q2 Find all users staying in Germany.

function getUsersStayingInGermany(users){
    if ((typeof (users) == 'object') && (!Array.isArray(users)) && (users != null)) {
        let usersStayingInGermany = [];
        for(let user in users){
            if(users[user].nationality == "Germany"){
                usersStayingInGermany.push(user);
            }
        }
        return usersStayingInGermany;
    }
    else{
        return [];
    }
}

module.exports = getUsersStayingInGermany;