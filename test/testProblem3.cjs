const users = require("../1-users.cjs");
const sortUsersBasedOnSeniority = require("../problem3.cjs");

try {
    let sortedUsersDetails = sortUsersBasedOnSeniority(users);
    console.log(sortedUsersDetails);
} catch (error) {
    console.log(error);
}