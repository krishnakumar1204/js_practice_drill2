const users = require("../1-users.cjs");
const getUsersInterestedInGames = require("../problem1.cjs");

try {
    let usersInterestedInGames = getUsersInterestedInGames(users);
    console.log(usersInterestedInGames);
} catch (error) {
    console.log("Something went wrong");
}