const users = require("../1-users.cjs")
const groupUsersBasedOnPLanguage = require("../problem5.cjs");

try {
    let groupedUsers = groupUsersBasedOnPLanguage(users);
    console.log(groupedUsers);
} catch (error) {
    console.log("Something went wrong");
}