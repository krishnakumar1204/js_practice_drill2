// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10

function sortUsersBasedOnSeniority(users) {
    if (typeof users == 'object' && !Array.isArray(users) && users != null) {

        let seniors = [];
        let developers = [];
        let interns = [];

        for(let user in users){
            if((users[user].designation).includes("Senior")){
                seniors.push(user);
            }
            else if((users[user].designation).includes("Developer")){
                developers.push(user);
            }
            else{
                interns.push(user);
            }
        }

        function sortBasedOnAge(userArray){
            for(let i=0; i<userArray.length-1; i++){
                for(let j=0; j<userArray.length-i-1; j++){
                    if(users[userArray[j]].age < users[userArray[j+1]].age){
                        let temp = userArray[j];
                        userArray[j] = userArray[j+1];
                        userArray[j+1] = temp;
                    }
                }
            }
        }

        sortBasedOnAge(seniors);
        sortBasedOnAge(developers);
        sortBasedOnAge(interns);


        let sortedUsersDetails = {};
        function fillDetails(userArray){
            for(let user of userArray){
                sortedUsersDetails[user] = users[user];
            }
        }
        fillDetails(seniors);
        fillDetails(developers);
        fillDetails(interns);

        return sortedUsersDetails;
    }
    else {
        return [];
    }
}

module.exports = sortUsersBasedOnSeniority;