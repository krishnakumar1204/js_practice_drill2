// Q1 Find all users who are interested in playing video games.

function getUsersInterestedInGames(users) {
    if ((typeof (users) == 'object') && (!Array.isArray(users)) && (users != null)) {
        let usersInterestedInGames = [];
        for (let user in users) {
            let interest = users[user].interests[0];
            if (interest.includes("Video Games")) {
                usersInterestedInGames.push(user);
            }
        }
        return usersInterestedInGames;
    }
    else{
        return [];
    }
}

module.exports = getUsersInterestedInGames;